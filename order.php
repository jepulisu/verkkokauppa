<?php include_once 'inc/top.php'; ?>
<div class="row">             
    <div class="col-xs-12 tilaus">
        <div class="row">
            <div class="col-xs-12">
                <h4>Tilaus</h4>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-8 col-lg-6">
                <form role="form" action="" method="post">
                    <div class="form-group">
                        <label>Nimi</label>
                        <input name="" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Lähiosoite</label>
                        <input name="" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Postitoimipaikka ja -numero</label>
                        <input name="" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Lähetä</button>
                    <button type="button" class="btn btn-default">Peruuta</button>
                </form>
            </div>
        </div>
    </div>
</div>   
<?php include_once 'inc/bottom.php'; ?>